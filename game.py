import pygame
import random
import math
pygame.init()
win=pygame.display.set_mode((1000, 800))
pygame.display.set_caption("River Crossing")
croc_number=4
level=1
croc_img=pygame.image.load('croc.png')
croc_X=[500, 500, 500, 500]
croc_Y=[90.9, 272.7, 454.5, 636.3]
croc_change=[1, 1, 1, 1]
rock_number=9
rock_img=pygame.image.load('rock.png')
rock_X=[142.86, 142.86, 142.86, 428.57, 428.57, 428.57, 714.28, 714.28, 714.28]
rock_Y=[181.8, 363.63, 545.45, 181.8, 363.63, 545.45, 181.8, 363.63, 545.45]
player_img=pygame.image.load('player.png')
player_X=500-30
player_Y=800-30
player_vel=20
def coll(player_X, player_Y, obst_X, obst_Y):
    distance_sqr=(math.pow(player_X - obst_X, 2) + math.pow(player_Y -obst_Y, 2))
    if distance_sqr < 4000:
        return True
    else:
        return False
def setup():
    blue=173,216,230
    win.fill(blue)
    pygame.draw.rect(win, (253, 184, 19), (0, 729.27, 1000, 90.9))
    pygame.draw.rect(win, (253, 184, 19), (0, 0, 1000, 90.9))
    pygame.draw.rect(win, (253, 184, 19), (0, 181.8, 1000, 90.9))
    pygame.draw.rect(win, (253, 184, 19), (0, 363.63, 1000, 90.9))
    pygame.draw.rect(win, (253, 184, 19), (0, 545.45, 1000, 90.9))
    i=0
    for i in range(rock_number):
        win.blit(rock_img, (rock_X[i], rock_Y[i]))
run=True
while run:
    pygame.time.delay(100)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run=False
    setup()
    i=0
    score=(level-1)*60
    croc_change[0]=18*level
    croc_change[1]=13*level
    croc_change[2]=12*level
    croc_change[3]=21*level
    for i in range(croc_number):
        croc_X[i]+=croc_change[i]
        if croc_X[i]>=1000:
            croc_X[i]=0
    pygame.display.update()
    keys=pygame.key.get_pressed()
    if keys[pygame.K_LEFT] and player_X>player_vel:
        player_X-=player_vel
    if keys[pygame.K_RIGHT] and player_X<1000-player_vel-30:
        player_X+=player_vel
    if keys[pygame.K_UP] and player_Y>player_vel:
        player_Y-=player_vel
    if keys[pygame.K_DOWN] and player_Y<800-player_vel-63:
        player_Y+=player_vel
    i=0
    for i in range(croc_number):
        win.blit(croc_img, (croc_X[i], croc_Y[i]))
    win.blit(player_img, (player_X, player_Y))
    i=0
    flag=0
    for i in range(rock_number):
        collison= coll(player_X+15, player_Y+15, rock_X[i]+45, rock_Y[i]+45)
        if collison:
            flag+=1
            break
    for i in range(croc_number):
        collison= coll(player_X+15, player_Y+15, croc_X[i]+45, croc_Y[i]+45)
        if collison:
            flag+=1
            break
    if flag==0:
        if player_Y <= 90.9:
            score+=5
        if player_Y <= 272.7:
            score+=5
        if player_Y <= 454.5:
            score+=5
        if player_Y <= 636.3:
            score+=5
        if player_Y <= 181.8:
            score+=10
        if player_Y <= 363.63:
            score+=10
        if player_Y <= 545.45:
            score+=10
        if player_Y <= 729.27:
            score+=10
        myFont= pygame.font.SysFont("Times New Roman", 18)
        display1=myFont.render("Score:" +str(score), 1, (0,0,0))
        win.blit(display1 , (0, 0))
        display3=myFont.render("Level:" +str(level), 1, (0, 0, 0))
        win.blit(display3, (940, 0))
        if player_Y < 80:
            white=255,255,255
            win.fill(white)
            font2= pygame.font.SysFont("Times New Roman", 50)
            display2=font2.render("Level Up", 1, (0, 0, 0))
            win.blit(display2, (400, 340))
            pygame.display.update()
            pygame.time.delay(1000)
            player_Y=770
            level+=1
    else:
        player_X=485
        player_Y=0
        white=255,255,255
        win.fill(white)
        level2=1
        font2= pygame.font.SysFont("Times New Roman", 50)
        display2=font2.render("2nd player turn", 1, (0, 0, 0))
        win.blit(display2, (400, 340))
        pygame.display.update()
        pygame.time.delay(1000)
        score2=0
        i=0
        score2=(level2-1)*60
        croc_change[0]=18*level2
        croc_change[1]=13*level2
        croc_change[2]=12*level2
        croc_change[3]=21*level2
        if player_Y >= 90.9:
            score2+=5
        if player_Y >= 272.7:
            score2+=5
        if player_Y >= 454.5:
            score2+=5
        if player_Y >= 636.3:
            score2+=5
        if player_Y >= 181.8:
            score2+=10
        if player_Y >= 363.63:
            score2+=10
        if player_Y >= 545.45:
            score2+=10
        if player_Y >= 729.27:
            score2+=10
        display1=myFont.render("Score:" +str(score2), 1, (0,0,0))
        win.blit(display1 , (0, 0))
        display3=myFont.render("Level:" +str(level2), 1, (0, 0, 0))
        win.blit(display3, (940, 0)) 
        if player_Y >= 730:
            white=255,255,255
            win.fill(white)
            font2= pygame.font.SysFont("Times New Roman", 50)
            display2=font2.render("Level Up", 1, (0, 0, 0))
            win.blit(display2, (400, 340))
            pygame.display.update()
            pygame.time.delay(1000)
            player_Y=0
            level2+=1
        i=0
        j=0
        k=0
        for i in range(rock_number):
            collison2= coll(player_X+15, player_Y+15, rock_X[i]+45, rock_Y[i]+45)
            if collison2:
                break
                k+=1
        for i in range(croc_number):
            collison2= coll(player_X+15, player_Y+15, croc_X[i]+45, croc_Y[i]+45)
            if collison2:
                break
                j+=1
        if j!=0 or k!=0:
            win.fill(white)
            font2= pygame.font.SysFont("Times New Roman", 50)
            if score2>score:
                display2=font2.render("Player 2 wins", 1, (0, 0, 0))
            else:
                display2=font2.render("Player 1 wins", 1, (0, 0, 0))
            win.blit(display2, (400, 340))
            pygame.display.update()
            pygame.time.delay(1000)
            break
    pygame.display.update()
pygame.quit()